// 37 <
// 39 >
// 32 space

function MyGalaga() {
    this.map = document.getElementById('myGalaga');
    this.ctx = this.map.getContext('2d');
    this.points = 0;

    this.shipBlockSize = 10;
    this.shipPositon = [
        {'x': 150, 'y': 480},
        {'x': 140, 'y': 490},
        {'x': 150, 'y': 490},
        {'x': 160, 'y': 490}
    ];
    this.bulletBlockSize = {'x': 6, 'y': 10};
    this.bulletPosition = new Array();
    this.enemyShipPosition = new Array();

    this.drawMap();
    this.drawShip();

    this.detectKey = this.detectKey.bind(this);
    document.onkeydown = this.detectKey;

    this.move = this.move.bind(this);
    this.moveTimer = setInterval(this.move, 16);

    this.generateEnemyShip = this.generateEnemyShip.bind(this);
    this.enemyShipTimer = setInterval(this.generateEnemyShip, 2000);

    this.detect = this.detect.bind(this);
    this.detectTimer = setInterval(this.detect, 1);
}

MyGalaga.prototype.clearMap = function() {
    this.ctx.clearRect(0, 0, this.map.width, this.map.height);
};

MyGalaga.prototype.drawMap = function() {
    this.map.width = 310;
    this.map.height = 500;
    this.map.style.display = 'block';
    this.map.style.margin = '0 auto';
    this.map.style.border = '1px solid';
};

MyGalaga.prototype.move = function() {
    this.moveBullet();
    this.moveEnemyShip();
    this.draw();
};

MyGalaga.prototype.draw = function() {
    this.clearMap();
    this.drawBullet();
    this.drawShip();
    this.drawEnemyShip();
};

MyGalaga.prototype.detect = function() {
    this.detectBulletCollision();
    this.detectEnemyShipCollision();
};

MyGalaga.prototype.drawShip = function() {
    var self = this;
    this.ctx.fillStyle = '#000';

    this.shipPositon.forEach(function(ship) {
        self.ctx.fillRect(ship.x, ship.y, self.shipBlockSize, self.shipBlockSize);
    });
};

MyGalaga.prototype.detectKey = function(event) {
    var key = event.keyCode;

    if (key == 37 || key == 39)
        this.moveShip(key);
    else if (key == 32)
        this.shoot();
};

MyGalaga.prototype.moveShip = function(key) {
    var self = this;

    if (!this.detectShipCollision(key)) {
        for (var i = 0; i < this.shipPositon.length; i++) {
            if (key == 37) {
                self.shipPositon[i].x = self.shipPositon[i].x - self.shipBlockSize;
            } else if (key == 39) {
                self.shipPositon[i].x = self.shipPositon[i].x + self.shipBlockSize;
            }
        }
    }
};

MyGalaga.prototype.detectShipCollision = function(key) {
    // ship on a wall
    if (key == 37) {
        if (this.shipPositon[1].x == 0) {
            return true;
        }
    } else if (key == 39) {
        if (this.shipPositon[3].x == 300)
            return true;
    }
};

MyGalaga.prototype.shoot = function() {
    this.bulletPosition.push({
        'x': this.shipPositon[0].x + 2,
        'y': this.shipPositon[0].y
    });
};

MyGalaga.prototype.drawBullet = function() {
    var self = this;
    this.ctx.fillStyle = '#ff0000';
    this.bulletPosition.forEach(function(bulletPosition) {
        self.ctx.fillRect(bulletPosition.x, bulletPosition.y, self.bulletBlockSize.x, self.bulletBlockSize.y);
    });
};

MyGalaga.prototype.moveBullet = function() {
    for (var i = 0; i < this.bulletPosition.length; i++) {
        this.bulletPosition[i].y = this.bulletPosition[i].y - 5
    }
};

MyGalaga.prototype.detectBulletCollision = function() {
    // detect hit in the top wall
    for (var i = 0; i < this.bulletPosition.length; i++) {
        if (this.bulletPosition[i].y == 0) {
            this.removeObject(this.bulletPosition, i);
        }
    }

    // detect hit with enemy ship
    for (var j = 0; j < this.bulletPosition.length; j++) { // loop on bullets position
        for (var k = 0; k < this.enemyShipPosition.length; k++) { // loop on enemy ships
            for (var l = 0; l < this.enemyShipPosition[k].length; l++) { // loop on enemy ship position
                if ((((this.bulletPosition[j].x >= this.enemyShipPosition[k][l].x) &&
                      (this.bulletPosition[j].x <= this.enemyShipPosition[k][l].x + this.shipBlockSize)) ||
                      ((this.bulletPosition[j].x + this.bulletBlockSize.x >= this.enemyShipPosition[k][l].x) &&
                      (this.bulletPosition[j].x + this.bulletBlockSize.x <= this.enemyShipPosition[k][l].x + this.shipBlockSize))) &&
                      (this.bulletPosition[j].y >= this.enemyShipPosition[k][l].y) &&
                      (this.bulletPosition[j].y <= this.enemyShipPosition[k][l].y + this.shipBlockSize)) {
                    this.removeObject(this.bulletPosition, j);
                    this.removeObject(this.enemyShipPosition, k);
                    this.countPoints();
                }
            }
        }
    }
};

MyGalaga.prototype.generateEnemyShip = function() {
    var shipPosition = Math.floor(Math.random() * 280);

    this.enemyShipPosition.push([
        {'x': shipPosition, 'y': 0},
        {'x': shipPosition + this.shipBlockSize, 'y': 0},
        {'x': shipPosition + (this.shipBlockSize * 2), 'y': 0},
        {'x': shipPosition + this.shipBlockSize, 'y': this.shipBlockSize}
    ]);
};

MyGalaga.prototype.moveEnemyShip = function() {
    for (var i = 0; i < this.enemyShipPosition.length; i++) {
        for (var j = 0; j < this.enemyShipPosition[i].length; j++) {
            this.enemyShipPosition[i][j].y = this.enemyShipPosition[i][j].y + 1;
        }
    }
};

MyGalaga.prototype.drawEnemyShip = function() {
    var self = this;
    this.ctx.fillStyle = '#000';
    self.ctx.fillRect(0, 0, self.bulletBlockSize, self.shipBlockSize);
    this.enemyShipPosition.forEach(function(enemyShipPosition) {
        enemyShipPosition.forEach(function(enemyShipPosition) {
            self.ctx.fillRect(enemyShipPosition.x, enemyShipPosition.y, self.shipBlockSize, self.shipBlockSize);
        })
    });
};

MyGalaga.prototype.detectEnemyShipCollision = function() {
    // detect hit in a bottom wall
    for (var i = 0; i < this.enemyShipPosition.length; i++) { // loop on enemy ships
        if (this.enemyShipPosition[i][0].y == this.map.height)
            this.removeObject(this.enemyShipPosition, i);
    }
    
    // detect hit with user ship
    for (var j = 0; j < this.shipPositon.length; j++) { // loop on bullets position
        for (var k = 0; k < this.enemyShipPosition.length; k++) { // loop on enemy ships
            for (var l = 0; l < this.enemyShipPosition[k].length; l++) { // loop on enemy ship position
                if ((((this.shipPositon[j].x >= this.enemyShipPosition[k][l].x) &&
                    (this.shipPositon[j].x <= this.enemyShipPosition[k][l].x + this.shipBlockSize)) ||
                    ((this.shipPositon[j].x + this.shipBlockSize >= this.enemyShipPosition[k][l].x) &&
                    (this.shipPositon[j].x + this.shipBlockSize <= this.enemyShipPosition[k][l].x + this.shipBlockSize))) &&
                    (this.shipPositon[j].y >= this.enemyShipPosition[k][l].y) &&
                    (this.shipPositon[j].y <= this.enemyShipPosition[k][l].y + this.shipBlockSize)) {
                    clearInterval(this.moveTimer);
                    clearInterval(this.enemyShipTimer);
                    clearInterval(this.detectTimer);
                    alert('Game over');
                }
            }
        }
    }
};

MyGalaga.prototype.countPoints = function() {
    this.points = this.points + 10;

    document.getElementById('points').innerHTML = this.points;
};

MyGalaga.prototype.removeObject = function(array, position) {
    array.splice(position, 1);
};