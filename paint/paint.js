function Paint() {
    this.createCanvas();
    this.draw = this.draw.bind(this);
    this.draw();
}

Paint.prototype.createCanvas = function() {
    this.canvas = document.querySelector('canvas');
    this.ctx = this.canvas.getContext('2d');

    this.canvas.width = parseInt(getComputedStyle(this.canvas).width);
    this.canvas.height = parseInt(getComputedStyle(this.canvas).height);

    this.canvasLeft = this.canvas.getBoundingClientRect().left;
    this.canvasTop = this.canvas.getBoundingClientRect().top;
};

Paint.prototype.draw = function() {
    this.clearCanvas();
    this.dragMove();
    this.drawByTool();

    requestAnimationFrame(this.draw);
};

Paint.prototype.clearCanvas = function() {
    var canvasWidth = this.canvas.width;
    var canvasHeight = this.canvas.height;

    this.ctx.fillStyle = '#fff';
    this.ctx.fillRect(0, 0, canvasWidth, canvasHeight)
};

Paint.prototype.dragMove = function() {
    var self = this;
    jQuery(this.canvas).on('mousedown', function(event) {
        event.preventDefault();
        var startX = event.pageX;
        var startY = event.pageY;
        self.mouseStartX = startX;
        self.mouseStartY = startY;
        self.mouseCurrentX = startX;
        self.mouseCurrentY = startY;

        jQuery(self.canvas).mousemove(function(event) {
            var currentX = event.pageX;
            var currentY = event.pageY;
            self.mouseCurrentX = currentX;
            self.mouseCurrentY = currentY;
        });
    });

    jQuery(this.canvas).on('mouseup', function(event) {
        event.stopPropagation();
        jQuery(self.canvas).off('mousemove');
        var endX = event.pageX;
        var endY = event.pageY;

        if (self.mouseCurrentX != endX)
            self.mouseEndX = endX;
        else
            self.mouseEndX = self.mouseCurrentX;

        if (self.mouseCurrentY != endY)
            self.mouseEndY = endY;
        else
            self.mouseEndY = self.mouseCurrentY;
    });
};

Paint.prototype.drawByTool = function() {
    this.ctx.beginPath();

    var startX = this.mouseStartX - this.canvasLeft;
    var startY = this.mouseStartY - this.canvasTop;
    var width = this.mouseCurrentX - this.mouseStartX;
    var height = this.mouseCurrentY - this.mouseStartY;

    this.ctx.moveTo(startX, startY);
    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(startX, startY, width, height);

    this.ctx.closePath();
};