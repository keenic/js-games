//37 <
//38 /\
//39 >
//40 \/

function Snake(){
    this.map = document.getElementById('snake');
    this.ctx = this.map.getContext('2d');
    this.previousDirection = 39;
    this.nextDirection = 39;
    this.snakeWidth = 8;
    this.snakeHeight = 8;
    this.points = 0;

    this.drawMap();

    this.snakePosition = [
        {'x': this.mapCenterX, 'y': this.mapCenterY},
        {'x': this.mapCenterX - 8, 'y': this.mapCenterY},
        {'x': this.mapCenterX - 16, 'y': this.mapCenterY},
        {'x': this.mapCenterX - 24, 'y': this.mapCenterY},
        {'x': this.mapCenterX - 32, 'y': this.mapCenterY}
    ];

    this.drawSnake();
    this.generateNewItem();

    this.moveSnake = this.moveSnake.bind(this);
    this.moveSnakeTimer = setInterval(this.moveSnake, 100);

    this.getNextMove = this.getNextMove.bind(this);
    document.onkeydown = this.getNextMove;
}

Snake.prototype.drawMap = function() {
    this.map.width = 400;
    this.map.height = 400;
    this.map.style.display = 'block';
    this.map.style.margin = '0 auto';
    this.map.style.border = '1px solid';

    this.mapCenterX = parseInt(this.map.width) / 2;
    this.mapCenterY = parseInt(this.map.height) / 2;
};

Snake.prototype.drawSnake = function() {
    var self = this;
    // draw Snake
    this.ctx.fillStyle = '#000';
    this.snakePosition.forEach(function(snakePosition) {
        self.ctx.fillRect(snakePosition.x, snakePosition.y, self.snakeWidth, self.snakeHeight);
    });
};

Snake.prototype.drawItem = function(positionX, positionY) {
    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(positionX, positionY, 8, 8);
}

Snake.prototype.getNextMove = function(event) {
    var key = event.keyCode;

    if (!this.keyPressed) {
        var changeDirection = false;
        if (key == 37 || key == 38 || key == 39 || key == 40) {
            if (key == 37 && this.nextDirection != 39)
                changeDirection = true;
            else if (key == 38 && this.nextDirection != 40)
                changeDirection = true;
            else if (key == 39 && this.nextDirection != 37)
                changeDirection = true;
            else if (key == 40 && this.nextDirection != 38)
                changeDirection = true;
            else
                changeDirection = false;

            if (changeDirection) {
                this.previousDirection = this.nextDirection;
                this.nextDirection = key;
            }
        }
    }

    this.keyPressed = true;
};

Snake.prototype.moveSnake = function() {
    var snakeHeadX = this.snakePosition[0].x;
    var snakeHeadY = this.snakePosition[0].y;

    if (this.nextDirection == 37 && this.previousDirection != 39) {
        this.snakePosition.unshift({
            'x': snakeHeadX - 8,
            'y': snakeHeadY
        });
        this.snakePosition.pop();
    } else if (this.nextDirection == 38 && this.previousDirection != 40) {
        this.snakePosition.unshift({
            'x': snakeHeadX,
            'y': snakeHeadY - 8
        });
        this.snakePosition.pop();
    } else if (this.nextDirection == 39 && this.previousDirection != 37) {
        this.snakePosition.unshift({
            'x': snakeHeadX + 8,
            'y': snakeHeadY
        });
        this.snakePosition.pop();
    } else if(this.nextDirection == 40 && this.previousDirection != 38) {
        this.snakePosition.unshift({
            'x': snakeHeadX,
            'y': snakeHeadY + 8
        });
        this.snakePosition.pop();
    }

    this.snakeHeadX = this.snakePosition[0].x;
    this.snakeHeadY = this.snakePosition[0].y;

    if (this.detecCollision()) {
        clearInterval(this.moveSnakeTimer);
        alert('Game over');
        return false;
    }

    // clear canvas to move snake
    this.ctx.clearRect(0, 0, this.map.width, this.map.height);

    this.drawItem(this.itemPositionX, this.itemPositionY);
    this.drawSnake();
    this.collectItem();

    this.keyPressed = false;
}

Snake.prototype.detecCollision = function() {
    var self = this;
    // stop move after hit a wall
    if (this.snakeHeadX == -8 || this.snakeHeadX == 400 || this.snakeHeadY == -8 || this.snakeHeadY == 400) {
        return true;
    }

    // stop move after hit snake
    for (var i = 1; i <= self.snakePosition.length - 1; i++) {
        if (self.snakeHeadX == self.snakePosition[i].x && self.snakeHeadY == self.snakePosition[i].y)
            return true;
    }
};

Snake.prototype.collectItem = function() {
    if (this.itemPositionX == this.snakeHeadX && this.itemPositionY == this.snakeHeadY) {
        this.generateNewItem();
        this.countPoints();
        this.snakePosition.push({
            'x': 0,
            'y': 0
        });
    }
};

Snake.prototype.generateNewItem = function() {
    var self = this;
    var newItemX = Math.floor(Math.random() * 392);
    var newItemY = Math.floor(Math.random() * 392);

    if (this.isInt(newItemX / 8) && this.isInt(newItemY / 8)) {
        this.snakePosition.forEach(function(snakePosition) {
            if(newItemX == snakePosition.x && newItemY == snakePosition.y)
                self.generateNewItem();
        });

        this.itemPositionX = newItemX;
        this.itemPositionY = newItemY;
        this.drawItem(this.itemPositionX, this.itemPositionY);
    } else {
        this.generateNewItem();
    }
};

Snake.prototype.countPoints = function() {
    var pointsContainer = document.getElementById('points');

    this.points = this.points + 10;
    pointsContainer.innerHTML = this.points;
};

Snake.prototype.isInt = function(n) {
    return n % 1 === 0;
};